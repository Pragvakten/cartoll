package se.hc.database.spotify.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import se.hc.database.spotify.Album;
import se.hc.database.spotify.Artist;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

/**
 * A repository for Spotify, storing artists and albums This class is tested by
 * SpotifyDaoTest. Run the test and implements all the needed logic.
 */
public class SpotifyDao {
	Logger log = Logger.getLogger(SpotifyDao.class);

	/**
	 * Returns a Connection to a database. Can be done in different ways. This
	 * methods shows you different ways of getting a Connection
	 */
	protected Connection getConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException,
			SQLException {

		String username = "root";
		String password = "";
		String servername = "localhost";
		String schemaName = "test";
		int port = 3306;
		String connectionUrl = "jdbc:mysql://" + servername + ":" + port + "/" + schemaName;
		Connection conn = null;

		int connectVersion = 3;

		switch (connectVersion) {

		case 1:
			// Option 1 - older version
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(connectionUrl, username, password);
			break;

		case 2:
			// Option 2 - A bit more modern
			Properties connectionProps = new Properties();
			connectionProps.put("user", username);
			connectionProps.put("password", password);
			conn = DriverManager.getConnection(connectionUrl, connectionProps);
			break;

		case 3:
			// A DataSource is normally the recommended way to get a Connection.
			// But the DataSource
			// should be created by the application server and not manually.
			// This is more common
			// in larger projects.
			MysqlDataSource ds = new MysqlDataSource();
			ds.setServerName(servername);
			ds.setDatabaseName(schemaName);
			ds.setUser(username);
			ds.setPassword(password);
			conn = ds.getConnection();
			break;

		default:

			throw new RuntimeException("No match for switch statement");
		}

		return conn;

	}

	public void init(Set<Artist> artists) throws Exception {

		// Clear all tables
		dropTable("albums");
		dropTable("artists");
		// Create tables
		String artistSql = "CREATE TABLE artists (id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, "
				+ "name varchar(50) UNIQUE)";
		executeSql(artistSql);

		String albumSql = "CREATE TABLE albums (id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,"
				+ "name varchar(50) UNIQUE," + "releaseYear year," + "artist_id INTEGER)";
		executeSql(albumSql);

		// Insert all artists into the database. First, only insert artists.
		// Later we will store albums for each artist.
		for (Artist artist : artists) {
			createArtist(artist.getId(), artist);
		}

	}

	private long createArtist(long id, Artist artist) throws Exception {
		String sql = "INSERT INTO artists (id, name) VALUES ('" + artist.getId() + "', '" + artist.getName() + "')";
		executeSql(sql);

		for (Album album : artist.getAlbums())
			createAlbum(album);

		return id;
	}

	public Artist getArtist(long artistId) throws Exception {

		ResultSetHandler resultSetHandler = new ResultSetHandler() {

			@Override
			public Object handleResultSet(ResultSet resultSet) throws Exception {
				Artist artist = null;
				while (resultSet.next()) {
					int id = resultSet.getInt("id");
					String string = resultSet.getString("name");
					artist = new Artist(id, string);
				}
				Set<Album> set = getAlbumsForArtist(artist);
				artist.setAlbums(set);
				return artist;
			}
		};

		String sql = "SELECT * FROM artists WHERE id=" + artistId;
		query(sql, resultSetHandler);
		return (Artist) resultSetHandler.getResult();

	}

	private void query(String sql, ResultSetHandler resultSetHandler) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		ResultSet resultSet = null;
		try {
			resultSet = statement.executeQuery(sql);
			resultSetHandler.handleResultSetInternal(resultSet);
		} catch (Exception e) {
			log.debug("Error: " + e.getMessage());
		} finally {
			connection.close();
			statement.close();
			resultSet.close();
		}
	}

	public Set<Album> getAlbumsForArtist(Artist artist) throws Exception {
		if (artist == null)
			return null;

		ResultSetHandler handler = new ResultSetHandler() {

			@Override
			public Object handleResultSet(ResultSet rs) throws Exception {
				HashSet<Album> result = new HashSet<Album>();
				while (rs.next()) {
					int id = rs.getInt("id");
					String name = rs.getString("name");
					int year = rs.getInt("releaseYear");
					Album album = new Album(id, name, year);
					result.add(album);
				}
				return result;
			}
		};

		String sql = "SELECT * FROM albums WHERE artist_id='" + artist.getId() + "'";
		query(sql, handler);
		Set<Album> set = (Set<Album>) handler.getResult();
		return set;
	}

	public void updateArtist(Artist artist) throws Exception {
		String sql = "UPDATE artists SET name='" + artist.getName() + "' WHERE id='" + artist.getId() + "'";
		executeSql(sql);
	}

	public void deleteArtist(long artistId) throws Exception {
		String sql = "DELETE FROM artists WHERE id='" + artistId + "'";
		executeSql(sql);
	}

	public int getArtistsSize() throws Exception {
		String sql = "SELECT count(*) AS numArtists FROM artists";
		ResultSetHandler resultSetHandler = new ResultSetHandler() {
			int result = 0;

			@Override
			public Object handleResultSet(ResultSet resultSet) throws Exception {
				while (resultSet.next()) {
					result = resultSet.getInt("numArtists");
				}
				return result;
			}
		};
		query(sql, resultSetHandler);
		return (Integer) resultSetHandler.getResult();
	}

	/**
	 * Creates an artist that has no ID. ID should be set automatically
	 */
	public long createArtist(Artist artist) throws Exception {
		String sql = "INSERT INTO artists (name) VALUES('" + artist.getName() + "')";
		executeSql(sql);
		return getMaxArtistId();
	}

	public long createAlbum(Album album) throws Exception {

		executeSql("INSERT INTO albums (id, name, artist_id, releaseYear) VALUES (" + album.getId() + ", '"
				+ album.getName() + "', " + album.getArtist().getId() + ", " + album.getReleaseYear() + ")");
		return album.getId();
	}

	/**
	 * Probably not a typical method, but represents the concept of updating
	 * multiple values in one transaction. If any update fails the whole
	 * transaction should be aborted.
	 * 
	 * Try using a PreparedStatement instead of a normal Statement
	 * 
	 */
	public void updateArtistNameAndAlbumName(Artist artist, String newArtistName, Album album, String newAlbumName)
			throws Exception {
		Connection connection = getConnection();
		String sqlArtist = "UPDATE artists SET name=? WHERE id=?";
		String sqlAlbum = "UPDATE albums SET name=? WHERE id=?";

		PreparedStatement preparedStatementArtist = connection.prepareStatement(sqlArtist);
		PreparedStatement preparedStatementAlbum = connection.prepareStatement(sqlAlbum);
		connection.setAutoCommit(false);

		try {
			preparedStatementArtist.setString(1, newArtistName);
			preparedStatementArtist.setLong(2, artist.getId());
			preparedStatementArtist.executeUpdate();

			preparedStatementAlbum.setString(1, newAlbumName);
			preparedStatementAlbum.setLong(2, album.getId());
			preparedStatementAlbum.executeUpdate();

			connection.commit();
		} catch (Exception e) {
			connection.rollback();
			throw e;

		} finally {
			connection.close();
			preparedStatementArtist.close();
			preparedStatementAlbum.close();
		}
	}

	protected void dropTable(String tableName) {
		try {
			executeSql("DROP TABLE " + tableName);

		} catch (Exception e) {
			log.debug("Error dropping table: " + tableName);
			log.debug(e.getMessage());
		}
	}

	protected void executeSql(String sql) throws Exception {
		log.debug("Executing sql: " + sql);
		Connection connection = getConnection();
		Statement statement = connection.createStatement();
		try {
			statement.executeUpdate(sql);

		} finally {
			connection.close();
			statement.close();
		}
	}

	/**
	 * Class that handles the logic of parsing the ResultSet. Different usecases
	 * will use a different parsing logic.
	 * 
	 */
	protected abstract class ResultSetHandler {

		private Object result;

		/**
		 * Called by the DAO. Simply stores the result in a variable for later
		 * retrieval.
		 */
		public void handleResultSetInternal(ResultSet rs) throws Exception {
			this.result = handleResultSet(rs);
		}

		/**
		 * The method that must be implemented by each usecase.
		 */
		public abstract Object handleResultSet(ResultSet rs) throws Exception;

		public Object getResult() {
			return result;
		}
	}

	/**
	 * Gets the highest ID for the Artists table
	 */
	public long getMaxArtistId() throws Exception {

		ResultSetHandler handler = new ResultSetHandler() {

			public Object handleResultSet(ResultSet rs) throws Exception {
				while (rs.next()) {
					int maxId = rs.getInt(1);
					return new Long(maxId);
				}
				return 0;
			}
		};

		query("select max(ID) from artists", handler);
		return (Long) handler.getResult();

	}
}
